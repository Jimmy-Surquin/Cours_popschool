# Cours SQL, partie 2 : PHP & MySQL 

12 juillet 2016, thomas HOCEDEZ (th@pop.eu.com) CC-BY-SA

## PHP et MySQL

### Pourquoi ? 

PHP permet de générer du HTML dynamiquement, mais il ne peut pas stocker des informations (BDD); Donc, c'est un SGBD qui va s'en coccuper.

Dans notre cas, on a choisi MySQL (MariaDB).

### Principe de Fonctionnement

1. PHP va créeer une connexion à une base de données. 
2. Avec cette connexion, on execute une requête.
3. PHP récupère le résultat (sous forme d'un tableau)
4. On ferme la connexion

## Connection

mysql_connect

1. Serveur de BDD
2. Username
3. Password

> $link = mysqli_connect("localhost", "my_user", "my_password", "test");

### Execution de requête 

On définit une variable pour stocker la requête:
> $requete="insert into eleve VALUES ('', '".$nom."','".$prenom."','".$adresse."')

On execute la requête et on récupère le résultat :
> $resultat=@mysql_query($requete,$bd); 

### Fermeture de la connexion 


## Retour sur les tableaux


### tableau 1 dimension :

>$fruits = array ("pommes","oranges","cerises");

> print_r($fruits);


>Array
>(
>    [0] => "Pommes"
>    [1] => "oranges"
>    [2] => "cerises"
>)


### tableau à 2 dimensions :

> $tableau = array('choix1' => 'rponse1', 'choix2' => 'reponse2');

> print_r($tableau);

>Array
>(
>    ["choix1"] => "rponse1"
>    ["Choix2"] => "reponse2"
>)
